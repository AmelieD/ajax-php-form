var form = document.querySelector('#form')

form.addEventListener('submit', function(e) {

    e.preventDefault()

    var data = new FormData(form)

    httpRequest.onreadystatechange = function() {

        if (httpRequest.readyState === 4) {

            if (httpRequest.status != 200) {

                var errors = JSON.parse(httpRequest.responseText)
            
                var errorsKey = Object.keys(errors)

                for ( var i = 0; i < errorsKey.length; i++) {

                    var key = errorsKey[i]

                    var error = errors[key]

                    var input = document.querySelector('[name=' + key + ']')

                    var span = document.createElement('span')

                    span.className = 'help-block'

                    span.innerHTML = error

                    input.parentNode.classList.add('has-errors')

                    input.parentNode.appendChild(span)

                } 

            } else {

                results = JSON.parse(httpRequest.responseText)

                console.log(results)
            
            }
  
        }

    }

    httpRequest.open('POST', form.getAttribute('action'), true )
    
    httpRequest.setRequestHeader('X-Requested-With', 'xmlhttprequest')

    httpRequest.send(data)
    
})
